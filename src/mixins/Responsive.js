export const Responsive = {
    data: function () {
        return {
            lkt: {
                responsive: {
                    windowWidth: 0,
                    windowHeight: 0,
                }
            },
        };
    },
    methods: {
        isMobile: function () {
            return this.windowWidth < 1024;
        },
        isLarge: function () {
            return this.windowHeight >= 1024;
        },
        getWindowWidth(event) {
            this.windowWidth = document.documentElement.clientWidth;
        },

        getWindowHeight(event) {
            this.windowHeight = document.documentElement.clientHeight;
        },
        addResponsiveEvent: function (){
            this.$nextTick(function() {
                window.addEventListener('resize', this.getWindowWidth);
                window.addEventListener('resize', this.getWindowHeight);

                //Init
                this.getWindowWidth();
                this.getWindowHeight();
            })
        },
    },
    beforeDestroy() {
        window.removeEventListener('resize', this.getWindowWidth);
        window.removeEventListener('resize', this.getWindowHeight);
    }
};
