export function Validation(name, fn) {
    this.name = name;
    this.evaluator = typeof fn === 'function' ? fn : (value) => { return true; };

    /**
     * @param value
     * @returns {boolean|*}
     */
    this.validate = (value) => {
        return this.evaluator(value);
    }
}