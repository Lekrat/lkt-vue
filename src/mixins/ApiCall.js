export const ApiCall = {
    methods: {

        api: function (data, props, cb, errorCb) {

            data = JSON.parse(JSON.stringify(data));

            // Parse provided data
            for (var key in props) {
                if (data.unsafeParams === true || (props.hasOwnProperty(key) && data.params.hasOwnProperty(key))) {
                    data.params[key] = props[key];
                }

                if (data.rename && data.rename[key] && (data.unsafeParams === true || (props.hasOwnProperty(key) && data.rename.hasOwnProperty(key)))){
                    data.params[data.rename[key]] = props[key];

                }
            }

            // Append fixed params
            for (var f in data.fixedParams) {
                if (data.fixedParams.hasOwnProperty(f)) {
                    data.params[f] = data.fixedParams[f];
                }
            }
            data.params._token = this.getToken();

            // Prepare link
            let link = data.link;
            if (data.method === 'get' || data.method === 'open') {
                let t = this.apiParamsToString(data.params);
                if (t.length > 0) {
                    link += '?' + t;
                }
            }

            link = this.fillString(link, data.params);

            if (!(typeof cb === 'function')) {
                cb = function (r) {
                    console.log(r);
                }
            }

            if (!(typeof errorCb === 'function')) {
                errorCb = function (r) {
                    console.log(r);
                }
            }

            let validateStatus = function (status) {
                return true;
            };

            // Execute call
            switch (data.method) {
                case 'get':
                case 'post':
                case 'delete':
                    return axios(
                        {
                            method: data.method,
                            url: link,
                            validateStatus: validateStatus,
                            data: data.params
                        }).then(r => {this.manageApiResponse(r, cb, errorCb)}).catch(error => {this.apiError(error, errorCb)});
                case 'open':
                    return axios.get(link, {'responseType': 'blob'}).then(r => {
                        window.download(r.data, data.name);
                        cb(r)
                    }).catch(error => this.apiError);
            }
        },

        manageApiResponse: function (r, cb, errorCb){
            if (r.status >= 200 && r.status < 300){
                cb(r);
            } else {
                this.apiError(r, errorCb);
            }
        },

        apiError: function (r, errorCb) {
            errorCb(r);
        },

        apiParamsToString: function (params) {
            let r = [];
            for (var key in params) {
                if (params.hasOwnProperty(key)) {
                    if (Array.isArray(params[key])) {
                        if (params[key].length > 0) {
                            r.push(key + '=' + JSON.stringify(params[key]) + '');
                        }
                    } else {
                        r.push(key + '=' + params[key]);
                    }
                }
            }

            // r.push('_token=' + this.getToken());
            return r.join('&');
        },

        convertArrayToString: function (data) {
            return '[' + data.map(datum => {
                return !isNaN(parseFloat(datum))
                && isFinite(datum) ? parseFloat(datum) : '"' + datum + '"';
            }).join(',') + ']';
        },

        getToken: function () {
            return window.token;
        },

        reloadPage: function () {
            window.location.reload();
        },

        redirectPage: function (location) {
            window.location.href = location;
        },

        getRequestUri: function(){
            let r = window.location.pathname;
            if (!r){
                r = '/';
            }
            return r;
        },
    }
};
