
import {Event} from "./classes/Event";
import {Validation} from "./classes/Validation";
import {Validator} from "./classes/Validator";

export const Lkt = {
    Event: Event,
    Validation: Validation,
    Validator: Validator,
};