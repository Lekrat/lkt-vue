export function Event(name, data) {
    this.name = name;
    this.data = data || {};
}